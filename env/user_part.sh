#!/usr/bin/env bash

yes | ./install_client.sh

rm compile_commands.json
ln -s /tmp/clippy-build/Debug/compile_commands.json compile_commands.json

rm ~/.vimrc
echo $(whoami)
ln -s $(realpath env/.vimrc) ~/.vimrc
git clone https://github.com/VundleVim/Vundle.vim.git ~/.vim/bundle/Vundle.vim
vim +PluginInstall +qall
cd ~/.vim/bundle/YouCompleteMe
python3 install.py --clangd-completer

