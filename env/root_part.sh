#!/usr/bin/env bash
yes | apt install build-essential cmake vim-nox python3-dev
yes | add-apt-repository ppa:jonathonf/vim
yes | apt update
yes | apt install vim

