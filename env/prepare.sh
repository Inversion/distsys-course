#!/bin/bash
docker_exec () {
    docker exec --user $1 --workdir /workspace/distsys-course distsys-course $2
}

docker_exec root ./env/root_part.sh
docker_exec $(whoami) ./env/user_part.sh
